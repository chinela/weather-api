    // Instantiate the UI class
    const ui = new UI();

    // Instantiate the Local Storage class
    const storage = new LS();

    // Get the city from the local storage
    const getCity = storage.getFromLs()



    // Instantiate the Weather class
    const weather = new Weather(getCity.city);

    console.log(getCity.city);

    document.addEventListener('DOMContentLoaded', loadDefaultWeather);

    document.querySelector('.form').addEventListener('submit', changeCity);

    ui.toggleSearch();
    ui.clearInputOnHide();
    ui.hideLoader();

    function loadDefaultWeather() {
        weather.getWeather()
        .then(res => {
            ui.populate(res);
            // console.log(res);
        })
        .catch(err => {
            if(err){
                console.log("there was an errroo")
            }
        })
    }

    function changeCity(e){
        const value = ui.getSearchedValue();
        $('input').val("");
        ui.hideSearch();
        
        // Create a loader
        ui.showLoader();
        // hide the wrapper
        ui.hideWrapper();

        weather.changeLocation(value);
        storage.addToLs(value)
        loadDefaultWeather();
        setTimeout(() => {
            // remove a loader
            ui.hideLoader();
            ui.showWrapper();
        }, 5000);

        e.preventDefault();
    }



// })
