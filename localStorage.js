class LS{
    constructor(){
        this.city;
        this.defaultCity = 'Abuja'
    }

    addToLs(city){
        localStorage.setItem('city', city);
    }

    getFromLs(){
        if(localStorage.getItem('city') === null){
            this.city = this.defaultCity;
        } else {
            this.city = localStorage.getItem('city');
        }

        return {
            city : this.city
        }
    }
}