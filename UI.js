class UI {
    constructor(){
        this.location = '.location';
        this.searchWrapper = '.search-wrapper';
        this.search = '#search';
        this.city = '.city';
        this.date = '.date';
        this.weather = '.weather';
        this.icon = '.icon';
        this.temp = '.temp';
        this.wind = '.wind';
        this.uv = '.uv';
        this.dew = '.dew';
        this.humidity = '.humidity'
        this.form = '.form';
        this.search = '#search';
        this.loader = '.loader';
        this.wrapper  = '.wrapper'
    }

    // hide the loader
    hideLoader(){
        $(this.loader).hide();
    }

    // show the loader
    showLoader(){
        $(this.loader).fadeIn();
    }

    // hide the content wrapper
    hideWrapper(){
        $(this.wrapper).hide()
    }

    // hide the content wrapper
    showWrapper(){
        $(this.wrapper).fadeIn()
    }

    // To google the search div 
    toggleSearch(){
        $(this.location).on('click', e => {
            $(this.searchWrapper).toggleClass('show');
        })
    }

    // To clear the input field when div hides
    clearInputOnHide(){
        $(this.location).on('click', e => {
            if(!$(this.searchWrapper).hasClass('show')){
                $(this.search).val("");
            }
        })
    }

    populate(data){
        $(this.city).html(data.location.region);
        $(this.date).html(Date());
        $(this.weather).html(data.current.condition.text);
        $(this.icon).attr('src', data.current.condition.icon);
        $(this.temp).html(data.current.temp_c + '<sup><span class="small ">o</span>c</sup>');
        $(this.dew).html(data.current.precip_in);
        $(this.uv).html(data.current.uv);
        $(this.humidity).html(data.current.humidity + '%');
        $(this.wind).html(data.current.wind_mph + ' mph');
    }

    getSearchedValue(){
        const value = $(this.search).val();

        return value;

    }

    hideSearch(){
        $(this.searchWrapper).removeClass('show');
    }

    // sea
}