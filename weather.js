class Weather{
    constructor(city) {
        // Instantiate a new instance of the api key
        // This class is ignored because it contains my api key
        let keys = new Key();

        // retrieve the api key
        this.apiKey = keys.apiKey();
        
        this.city = city;
    }

    async getWeather(){
        const response = await fetch(`https://api.apixu.com/v1/current.json?key=${this.apiKey}&q=${this.city}`);
        console.log(response);

        const resData = await response.json();

        return resData;
    }

    changeLocation(city){
        this.city = city;
    }
}